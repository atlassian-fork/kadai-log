import sbt._
import sbt.Keys._
import Settings._
import Dependencies._

trait Modules {
  lazy val core =
    module("core")

  lazy val json =
    module("json").dependsOn(core)

  lazy val jsonCirce =
    module("json-circe").dependsOn(core)

  lazy val all = 
    module(
      id = "all"
    , fileName = Some(".")
    ) dependsOn ( // needs both dependsOn and aggregate to produce dependencies in the pom
      core, json, jsonCirce
    ) aggregate (
      core, json, jsonCirce
    )

  def module(id: String, fileName: Option[String] = None) = 
    Project(
      id = id
    , base = file(fileName.getOrElse(id))
    , settings = standardSettings
    ).settings(
      name := s"kadai-logging-$id"
    , libraryDependencies ++= commonTest
    )
}
