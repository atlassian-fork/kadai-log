package kadai.log.log4j

import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.{ LogEvent => Log4jLogEvent }
import org.apache.logging.log4j.core.config.Property
import org.apache.logging.log4j.core.impl.{ Log4jLogEvent => Log4jLogEventImpl }
import org.apache.logging.log4j.message.Message
import collection.JavaConverters._

object LogEvent {
  def apply(name: String, msg: Message, lvl: Level = Level.INFO, fqcn: String = "fqcn"): Log4jLogEvent =
    new Log4jLogEventImpl(name, null, fqcn, lvl, msg, List[Property]().asJava, null)
}
