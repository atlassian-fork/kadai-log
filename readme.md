# kadai-log

## Simple but flexible log facade 

Lightweight Scala friendly logging library, currently uses log4j2 under the covers

Contains support for formatting logs as json.

### JSON Logging

Note that from 4.0, the default format for JSON messages has changed so that the standard qualifier
for messages is the simple class name of the thing being logged.

The JSON that is output contains the following by default in the JSON message payload:

    { 
      "time" : "2015-09-07T04:36:49.881+0000",
      "logger" : "com.foo.MyClass",
      "level" : "INFO",
      "ctx" : null,
      "name": jsonPayload
    }

where `name` is the qualified name of the object being logged.

The logged object needs to have a qualified name so as to give a unique path to all the fields
so log aggregators such as ElasticSearch/Kibana can index them with a consistent schema. If they
do not have unique paths, they may [fail to be indexed at all](https://github.com/elastic/elasticsearch/issues/12366).

By default the unique path is either `msg` for simple `String` logs, or the simple name of the
class that it being logged, eg: `Throwable` for exceptions, `AccessLog` for access logs,
`Invalid` for `kadai.Invalid` or whatever the logged object is.

The qualified name can be overriden and customised by providing a `kadai.log.json.JsonMessage.Qualified[A]`
for your type `A` that you want to log, that controls how it is encoded into JSON tuples. There are
convenience methods there that take a String and an EncodeJson for simple name overloading, 
or a function that gives complete control over the top-level names used (although care should be taken).
Usually these should be provided on the companion object to the class being logged.

# Download

To use this library from SBT, add the following library dependency:

    libraryDependencies += "io.atlassian" %% "kadai-logging" % "6.0.0"

There is an artifactId: `kadai-logging` that combines all modules or you can depend on one directly with `kadai-logging-core`, `kadai-logging-json` etc. Note:, there is a scala-version qualifier.

Version 5.x works with scalaz 7.1, Version 6.0,x works with scalaz 7.2 (and currently argonaut 6.1a).
