package kadai.log

import org.specs2.Specification
import scalaz.Show

object LoggingSpec extends Specification with Logging {
  import Logging._

  def is = s2"""
  Logging must 
   
     derive the LogWriter from a Show instance $testWithInfo
     prefer an explicit LogWriter to a Show    $logWriterPrecedence
  """

  def testWithInfo =
    withInfo("string") { 3 } === 3

  def logWriterPrecedence =
    withInfo(Foo(7)) { 3 } === 3

  case class Foo(i: Int)
  object Foo {
    implicit val FooLogWriter: LogWriter[Foo] = new LogWriter[Foo] {
      def apply(a: => Foo) = new LogMessage[Foo] {
        def value = a
        /** for producing the message String */
        def show = Show.showFromToString
      }

    }
    implicit val FooShow = new Show[Foo] {
      override def shows(f: Foo): String = ???
    }
  }
}