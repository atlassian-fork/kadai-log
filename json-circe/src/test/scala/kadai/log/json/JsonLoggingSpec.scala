package kadai.log
package json

import io.circe.Encoder
import org.scalacheck.Prop
import org.specs2.{ Specification, ScalaCheck }

object JsonLoggingSpec extends Specification with ScalaCheck with JsonLogging {
  import JsonLogging._

  def is = s2"""
   JsonLogging should
     standardise on 'msg' for a String                  $logString
     standardise on 'Throwable' for a Throwable         $logThrowable
     log both for a tuple                               $logTuple
     only log for a defined option                      $logOption
     prefer an explicit Encoder to a Show               $logWriterPrecedence
     default to class name for backwards compatibility  $logWriterDefaultQualifier
   """

  def logString =
    Prop.forAll { (s: String) =>
      LogWriter[String].apply(s) should beLike {
        case JsonMessage(_, List((q, ss))) => (q === "msg") and (ss === Encoder[String].apply(s))
      }
    }

  def logThrowable =
    Prop.forAll { (t: Throwable) =>
      LogWriter[Throwable].apply(t) should beLike {
        case JsonMessage(_, List((q, tt))) => (q === "Throwable") and (tt === Encoder[Throwable].apply(t))
      }
    }

  def logTuple =
    Prop.forAll { (s: String, t: Throwable) =>
      LogWriter[(String, Throwable)].apply(s -> t) should beLike {
        case JsonMessage(_, List((qs, ss), (qt, tt))) =>
          (qs === "msg") and (ss === Encoder[String].apply(s)) and
            (qt === "Throwable") and (tt === Encoder[Throwable].apply(t))
      }
    }

  def logOption =
    Prop.forAll { (s: Option[String]) =>
      LogWriter[Option[String]].apply(s) should beLike {
        case JsonMessage(_, List()) => s.isDefined === false
        case JsonMessage(_, List((_, ss))) => ss === Encoder[String].apply(s.get)
      }
    }

  case class Foo(i: Int)
  object Foo {
    implicit val FooEncoder: Encoder[Foo] =
      implicitly[Encoder[Int]].contramap { _.i }

    implicit val FooQualified = JsonMessage.Qualified.by[Foo]("foo")
  }
  def logWriterPrecedence = {
    LogWriter[Foo].apply(Foo(1)) should beLike {
      case JsonMessage(_, List((q, _))) => q === "foo"
    }
  }

  /** this needs to be outside of the test method otherwise an InternalError is thrown */
  case class Bar(i: Int)
  object Bar {
    implicit val FooEncoder: Encoder[Bar] =
      implicitly[Encoder[Int]].contramap { _.i }
  }

  def logWriterDefaultQualifier =
    LogWriter[Bar].apply(Bar(1)) should beLike {
      case JsonMessage(_, List((q, _))) => q === "Bar"
    }
}
